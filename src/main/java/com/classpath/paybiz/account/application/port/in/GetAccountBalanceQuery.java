package com.classpath.paybiz.account.application.port.in;

import com.classpath.paybiz.account.domain.Account;
import com.classpath.paybiz.account.domain.Money;

public interface GetAccountBalanceQuery {

	Money getAccountBalance(Account.AccountId accountId);

}
