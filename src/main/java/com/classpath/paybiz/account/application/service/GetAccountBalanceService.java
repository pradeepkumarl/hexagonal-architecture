package com.classpath.paybiz.account.application.service;

import java.time.LocalDateTime;

import com.classpath.paybiz.account.application.port.in.GetAccountBalanceQuery;
import com.classpath.paybiz.account.domain.Account;
import com.classpath.paybiz.account.domain.Money;
import com.classpath.paybiz.account.application.port.out.LoadAccountPort;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class GetAccountBalanceService implements GetAccountBalanceQuery {

	private final LoadAccountPort loadAccountPort;

	@Override
	public Money getAccountBalance(Account.AccountId accountId) {
		return loadAccountPort.loadAccount(accountId, LocalDateTime.now())
				.calculateBalance();
	}
}
