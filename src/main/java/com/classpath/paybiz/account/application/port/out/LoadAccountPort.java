package com.classpath.paybiz.account.application.port.out;

import java.time.LocalDateTime;

import com.classpath.paybiz.account.domain.Account;

public interface LoadAccountPort {

	Account loadAccount(Account.AccountId accountId, LocalDateTime baselineDate);
}
