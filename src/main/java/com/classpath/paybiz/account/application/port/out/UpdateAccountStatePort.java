package com.classpath.paybiz.account.application.port.out;

import com.classpath.paybiz.account.domain.Account;

public interface UpdateAccountStatePort {

	void updateActivities(Account account);

}
