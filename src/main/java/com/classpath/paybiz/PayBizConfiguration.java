package com.classpath.paybiz;

import com.classpath.paybiz.account.application.service.MoneyTransferProperties;
import com.classpath.paybiz.account.domain.Money;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(PayBizConfigurationProperties.class)
public class PayBizConfiguration {

  /**
   * Adds a use-case-specific {@link MoneyTransferProperties} object to the application context. The properties
   * are read from the Spring-Boot-specific {@link PayBizConfigurationProperties} object.
   */
  @Bean
  public MoneyTransferProperties moneyTransferProperties(PayBizConfigurationProperties payBizConfigurationProperties){
    return new MoneyTransferProperties(Money.of(payBizConfigurationProperties.getTransferThreshold()));
  }

}
